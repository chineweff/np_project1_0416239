#include <iostream>
#include <string>
#include <map>
#include <unistd.h>
#include <sys/wait.h>
#include <sstream>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
using namespace std;
string filename;
char go;
int pipe_num, command_num, newsockfd, sockfd;
bool first_command, command_not_found;
map<int, string>find_tmp;
void launch(char** command)
{
    if((string)command[0] == "exit")
    {
        for(auto &it : find_tmp)
            remove(it.second.c_str());
        close(newsockfd);
        exit(0);
    }
    if((string)command[0] == "setenv")
    {
        setenv(command[1], command[2], true);
        return;
    }
    else if((string)command[0] == "printenv")
    {
        cout << command[1] << "=" << getenv(command[1]) << endl;
        return;
    }

    if(go == '|' && find_tmp.find(pipe_num+command_num) == find_tmp.end() )
    {
        int fd;
        char tmpfile[] = {"/tmp/0416239XXXXXX"};
        fd = mkstemp(tmpfile);
        find_tmp.insert(pair<int, string >(pipe_num+command_num, (string)tmpfile));
        close(fd);
    }

    pid_t pid, wpid;
    int status;
    pid = fork();

    if( pid == 0) // child
    {
        if(go == '|') freopen(find_tmp[command_num+pipe_num].c_str(), "a", stdout);

        if(go == '>') freopen(filename.c_str(), "w", stdout);

        if( find_tmp.find(command_num) != find_tmp.end() ) freopen(find_tmp[command_num].c_str(), "r", stdin);

        if( execvp(command[0], command) == -1 )
        {
            if(go == '|')
            {
                struct stat statbuf;
                stat(find_tmp[command_num+pipe_num].c_str(), &statbuf);
                if(statbuf.st_size == 0)
                {
                    remove(find_tmp[command_num+pipe_num].c_str());
                    find_tmp.erase(command_num+pipe_num);
                }
            }
            else if(go == '>') remove(filename.c_str());
            dup2(newsockfd, STDOUT_FILENO);
            cout << "Unknown command" << ": [" << (string)command[0] << "]." << endl;
        }
        exit(EXIT_FAILURE);
    }
    else if ( pid < 0)
        perror("fork error");

    else // parent
    {
        int receive_status;
        waitpid(pid, &status, 0);
        if(WIFEXITED(status))  receive_status = WEXITSTATUS(status);

        if(receive_status == EXIT_FAILURE)
        {
            command_not_found = true;
            if(!first_command) --command_num;
        }

        if(find_tmp.find(command_num) != find_tmp.end() )
        {
            remove(find_tmp[command_num].c_str());
            find_tmp.erase(command_num);
        }
    }
}
void part(vector<char*> parse)
{
    if(parse.empty()) return;
    command_num++;
    parse.push_back(NULL);
    char** command = parse.data();
    launch(command);
    first_command = false;
}
void shell()
{
    string ini_dir = (string)getenv("HOME") + "/ras";
    chdir(ini_dir.c_str());
    setenv("PATH", "bin:." , true);
    cout << "****************************************" << endl;
    cout << "** Welcome to the information server. **" << endl;
    cout << "****************************************" << endl;
    string input;
    while( (cout << "% ").flush() && getline(cin, input) )
    {
        if(input.empty()) continue;
        vector<char*> parse;
        stringstream ss(input);
        command_not_found = false;
        first_command = true;
        while(1)
        {
            if(command_not_found) break;
            string tmp;
            ss >> tmp;
            if(tmp == "")
            {
                go = '\0';
                part(parse);
                break;
            }
            if(tmp[0] == '|')
            {
                go = tmp[0];
                tmp.erase(0,1);
                pipe_num = tmp.empty() ? 1 : stoi(tmp);
                part(parse);
                parse.clear();
                continue;
            }
            if(tmp == ">")
            {
                go = tmp[0];
                ss >> filename;
                part(parse);
                break;
            }
            char* qq = new char[tmp.length()+1];
            strcpy(qq, tmp.c_str());
            parse.push_back(qq);
        }
    }
}
void child_exit(int)
{
    wait(nullptr);
}
void parent_exit(int)
{
    for(auto &it : find_tmp) remove(it.second.c_str());
    close(sockfd);
    close(newsockfd);
    exit(0);
}
int main(int argc, char *argv[])
{
     signal (SIGCHLD, child_exit);
     signal (SIGINT, parent_exit);
     socklen_t clilen;
     struct sockaddr_in serv_addr, cli_addr;
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) perror("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(9999);
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
         perror("ERROR on binding");
     listen(sockfd,5);
     while(1)
     {
        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        pid_t pid;
        pid = fork();
        if( pid == 0)
        {
            close(sockfd);
            dup2(newsockfd, STDOUT_FILENO);
            dup2(newsockfd, STDIN_FILENO);
            dup2(newsockfd, STDERR_FILENO);
            shell();
        }
        else
        {
            close(newsockfd);
        }
     }
     return 0;
}
